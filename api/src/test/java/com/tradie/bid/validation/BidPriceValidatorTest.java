package com.tradie.bid.validation;

import com.tradie.bid.modal.Bid;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BidPriceValidatorTest {
    @Test
    public void testValidator() {
        BidPriceValidator v = new BidPriceValidator();
        assertFalse(v.isValid(Bid.builder().build(), null));
        assertFalse(v.isValid(Bid.builder().hourlyPrice(10.00).fixedPrice(100.00).build(), null));
        assertTrue(v.isValid(Bid.builder().hourlyPrice(10.00).build(), null));
        assertTrue(v.isValid(Bid.builder().fixedPrice(10.00).build(), null));
    }
}
