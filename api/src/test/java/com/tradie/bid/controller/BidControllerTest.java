package com.tradie.bid.controller;

import com.tradie.bid.modal.Bid;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BidControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testGetAllBidsForProject() {
        webTestClient.get()
                .uri("/api/projects/1/bids")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$")
                .isArray();
    }

    @Test
    public void testNewBid() {
        webTestClient.post()
                .uri("/api/projects/1/bids")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Bid.builder().fixedPrice(200.00).build())
                .exchange()
                .expectStatus()
                .is2xxSuccessful();
    }

    @Test
    public void testNewBidValidate() {
        webTestClient.post()
                .uri("/api/projects/1/bids")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Bid.builder().fixedPrice(200.00).hourlyPrice(20.00).build())
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .jsonPath("$.errors")
                .isArray()
                .jsonPath("$.errors")
                .value(e -> {
                    assertEquals(1, e.size());
                    assertTrue(e.contains("Only one of fixedPrice or hourlyPrice must be provided"));
                }, List.class);

        webTestClient.post()
                .uri("/api/projects/1/bids")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Bid.builder().build())
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .jsonPath("$.errors")
                .isArray()
                .jsonPath("$.errors")
                .value(e -> {
                    assertEquals(1, e.size());
                    assertTrue(e.contains("Only one of fixedPrice or hourlyPrice must be provided"));
                }, List.class);
    }

    @Test
    public void tetsUpdateBid() {
        webTestClient.put()
                .uri("/api/bids/1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Bid.builder().hourlyPrice(40.00).build())
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.fixedPrice")
                .doesNotExist();
    }
}
