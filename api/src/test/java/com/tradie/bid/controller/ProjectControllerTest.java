package com.tradie.bid.controller;

import com.tradie.bid.modal.Project;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProjectControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testCreateProjectSuccess() {
        webTestClient.post()
                .uri("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Project.builder()
                        .title("test title")
                        .description("test description")
                        .hours(10)
                        .lastBidDate(LocalDate.now().plusDays(20))
                        .build())
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id").isNumber()
                .jsonPath("$.status").isEqualTo("ACTIVE");
    }

    @Test
    public void testCreateProjectValidation() {
        webTestClient.post()
                .uri("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Project.builder().build())
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .jsonPath("$.errors")
                .isArray()
                .jsonPath("$.errors")
                .value(e -> {
                    assertEquals(4, e.size());
                    assertTrue(e.contains("title is required"));
                    assertTrue(e.contains("description is required"));
                    assertTrue(e.contains("hours is required"));
                    assertTrue(e.contains("lastBidDate is required"));
                }, List.class);

        webTestClient.post()
                .uri("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Project.builder()
                        .title("test title")
                        .description("test description")
                        .hours(10)
                        .lastBidDate(LocalDate.now().minusDays(1))
                        .build())
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .jsonPath("$.errors")
                .isArray()
                .jsonPath("$.errors")
                .value(e -> {
                    assertEquals(1, e.size());
                    assertTrue(e.contains("lastBidDate must be in future"));
                }, List.class);
    }

    @Test
    public void testAllProjects() {
        webTestClient.get()
                .uri("/api/projects")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.content")
                .isArray();
    }

    @Test
    public void testUpdateProject() {
        webTestClient.put()
                .uri("/api/projects/1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Project.builder()
                        .title("test updated")
                        .description("test updated")
                        .hours(10)
                        .lastBidDate(LocalDate.now().plusDays(20))
                        .build())
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.title").isEqualTo("test updated")
                .jsonPath("$.description").isEqualTo("test updated")
                .jsonPath("$.hours").isEqualTo(10);
    }

    @Test
    public void getProject() {
        webTestClient.get()
                .uri("/api/projects/1")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id")
                .isEqualTo(1);
    }
}
