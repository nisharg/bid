package com.tradie.bid.service;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.modal.Project;
import com.tradie.bid.repository.BidRepository;
import com.tradie.bid.repository.ProjectRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class BidServiceTest {
    @InjectMocks
    private BidService bidService;

    @Mock
    private BidRepository bidRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Test
    public void testAllBidsByProject() {
        when(bidRepository.findByProjectId(1L)).thenReturn(List.of(Bid.builder().build()));
        List<Bid> bids = bidService.all(1L);
        assertEquals(bids.size(), 1);
    }

    @Test
    public void testSaveBidSuccess() {
        when(projectRepository.findById(1L)).thenReturn(Optional.of(Project.builder().build()));

        ArgumentCaptor<Bid> captor = ArgumentCaptor.forClass(Bid.class);
        when(bidRepository.save(captor.capture())).thenReturn(Bid.builder().build());
        Bid input = Bid.builder()
                .fixedPrice(100.0)
                .build();
        Bid bid = bidService.save(1L, input);
        assertEquals(input.getFixedPrice(), captor.getValue().getFixedPrice());
    }

    @Test
    public void testSaveBidProjectNotFound() {
        when(projectRepository.findById(any())).thenReturn(Optional.empty());

        RuntimeException thrown = assertThrows(RuntimeException.class, () -> bidService.save(1L, Bid.builder().build()));
        assertEquals("Project not found", thrown.getMessage());
    }

    @Test
    public void testUpdateBidSuccess() {
        ArgumentCaptor<Bid> captor = ArgumentCaptor.forClass(Bid.class);
        when(bidRepository.findById(1L)).thenReturn(Optional.of(Bid.builder().build()));
        when(bidRepository.save(captor.capture())).thenReturn(Bid.builder().build());
        Bid input = Bid.builder()
                .hourlyPrice(10.0)
                .build();
        Bid bid = bidService.update(1L, input);
        assertEquals(input.getHourlyPrice(), captor.getValue().getHourlyPrice());
    }

    @Test
    public void testUpdateBidNotFound() {
        when(bidRepository.findById(any())).thenReturn(Optional.empty());
        RuntimeException thrown = assertThrows(RuntimeException.class, () -> bidService.update(1L, Bid.builder().build()));
        assertEquals("Bid not found", thrown.getMessage());
    }
}
