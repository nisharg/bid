package com.tradie.bid.service;

import com.tradie.bid.modal.Project;
import com.tradie.bid.modal.ProjectStatus;
import com.tradie.bid.repository.ProjectRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class ProjectServiceTest {
    @InjectMocks
    private ProjectService projectService;

    @Mock
    private ProjectRepository projectRepository;

    @Test
    public void testSaveProject() {
        Project input = Project.builder()
                .title("title")
                .description("description")
                .hours(10)
                .lastBidDate(LocalDate.now().plusDays(10))
                .build();

        ArgumentCaptor<Project> captor = ArgumentCaptor.forClass(Project.class);
        when(projectRepository.save(captor.capture())).thenReturn(input);
        Project project = projectService.save(input);
        Project captorValue = captor.getValue();
        Assertions.assertEquals(captorValue.getStatus(), ProjectStatus.ACTIVE);
        Assertions.assertEquals(captorValue.getTitle(), project.getTitle());
        Assertions.assertEquals(captorValue.getDescription(), project.getDescription());
        Assertions.assertEquals(captorValue.getHours(), project.getHours());
        Assertions.assertEquals(captorValue.getLastBidDate(), project.getLastBidDate());
    }

    @Test
    public void updateProjectSuccess() {
        Project input = Project.builder()
                .title("title")
                .description("description")
                .hours(10)
                .lastBidDate(LocalDate.now().plusDays(10))
                .build();

        when(projectRepository.findById(1L)).thenReturn(Optional.of(Project.builder()
                .id(1L)
                .title("old-title")
                .description("old-description")
                .hours(1)
                .lastBidDate(LocalDate.now().plusDays(1))
                .status(ProjectStatus.ACTIVE)
                .build()));

        ArgumentCaptor<Project> captor = ArgumentCaptor.forClass(Project.class);
        when(projectRepository.save(captor.capture())).thenReturn(input);

        Project project = projectService.update(1L, input);
        Project captorValue = captor.getValue();
        Assertions.assertEquals(captorValue.getTitle(), input.getTitle());
        Assertions.assertEquals(captorValue.getDescription(), input.getDescription());
        Assertions.assertEquals(captorValue.getHours(), input.getHours());
        Assertions.assertEquals(captorValue.getLastBidDate(), input.getLastBidDate());
    }

    @Test
    public void testUpdateProjectNotActive() {
        Project input = Project.builder()
                .title("title")
                .description("description")
                .hours(10)
                .lastBidDate(LocalDate.now().plusDays(10))
                .build();

        when(projectRepository.findById(1L)).thenReturn(Optional.of(Project.builder()
                .id(1L)
                .title("old-title")
                .description("old-description")
                .hours(1)
                .lastBidDate(LocalDate.now().plusDays(1))
                .status(ProjectStatus.CLOSED)
                .build()));

        RuntimeException thrown = assertThrows(RuntimeException.class, () -> projectService.update(1L, input));

        Assertions.assertEquals("Project can not be updated", thrown.getMessage());
    }

    @Test
    public void testUpdateProjectNotFound() {
        Project input = Project.builder()
                .title("title")
                .description("description")
                .hours(10)
                .lastBidDate(LocalDate.now().plusDays(10))
                .build();

        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException thrown = assertThrows(RuntimeException.class, () -> projectService.update(1L, input));

        Assertions.assertEquals("Project not found", thrown.getMessage());
    }
}
