package com.tradie.bid.schedule;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.modal.BidStatus;
import com.tradie.bid.modal.Project;
import com.tradie.bid.modal.ProjectStatus;
import com.tradie.bid.repository.BidRepository;
import com.tradie.bid.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class BidSelectorTest {
    @InjectMocks
    private BidSelector bidSelector;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private BidRepository bidRepository;

    @Test
    public void testSchedulerNoBids() {
        when(bidRepository.findByProjectLastBidDateLessThanEqualAndProjectStatusIs(any(), any()))
                .thenReturn(List.of());

        bidSelector.schedule();

        verify(bidRepository).findByProjectLastBidDateLessThanEqualAndProjectStatusIs(any(LocalDate.class), eq(ProjectStatus.ACTIVE));
        verifyNoMoreInteractions(bidRepository);
        verifyNoInteractions(projectRepository);
    }

    @Test
    public void testScheduler() {
        Project project = Project.builder().id(1L)
                .title("test")
                .description("desc")
                .lastBidDate(LocalDate.now().plusDays(20))
                .hours(20)
                .build();

        List<Bid> bids = List.of(Bid.builder().id(1L).project(project).fixedPrice(150.0).build(),
                Bid.builder().id(2L).project(project).hourlyPrice(20.0).build());

        when(bidRepository.findByProjectLastBidDateLessThanEqualAndProjectStatusIs(any(), any()))
                .thenReturn(bids);

        bidSelector.schedule();

        verify(bidRepository).findByProjectLastBidDateLessThanEqualAndProjectStatusIs(any(LocalDate.class), eq(ProjectStatus.ACTIVE));

        verify(bidRepository).updateBidStatus(BidStatus.SELECTED, 1L);
        verify(bidRepository).updateBidStatus(BidStatus.REJECTED, 2L);
        verify(projectRepository).updateProjectStatus(ProjectStatus.CLOSED, 1L);
    }
}
