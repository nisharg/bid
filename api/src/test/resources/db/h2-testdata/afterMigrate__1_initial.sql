insert into `project`(`id`, `title`, `description`, `hours`, `last_bid_date`, `status`)
values(1, 'title', 'description', 100, '2022-02-20', 'ACTIVE');

insert into `bid`(`id`, `project_id`, `fixed_price`, `hourly_price`, `status`)
values(1, 1, 123.00, null, 'ACTIVE');

insert into `bid`(`id`, `project_id`, `fixed_price`, `hourly_price`, `status`)
values(2, 1, null, 50.00, 'ACTIVE');