package com.tradie.bid.validation;

import com.tradie.bid.modal.Bid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BidPriceValidator implements ConstraintValidator<ValidateBidPrice, Bid> {
    @Override
    public boolean isValid(Bid bid, ConstraintValidatorContext context) {
        if (bid == null) {
            return false;
        }

        if (bid.getFixedPrice() == null && bid.getHourlyPrice() == null) {
            return false;
        }

        if (bid.getFixedPrice() != null && bid.getHourlyPrice() != null) {
            return false;
        }

        return true;
    }
}
