package com.tradie.bid.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = BidPriceValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateBidPrice {
    String message() default "Only one of fixedPrice or hourlyPrice must be provided";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
