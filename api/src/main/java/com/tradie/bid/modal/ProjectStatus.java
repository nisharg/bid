package com.tradie.bid.modal;

public enum ProjectStatus {
    ACTIVE,
    CLOSED,
    REMOVED
}
