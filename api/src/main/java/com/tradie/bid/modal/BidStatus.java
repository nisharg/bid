package com.tradie.bid.modal;

public enum BidStatus {
    ACTIVE,
    WITHDRAWN,
    REJECTED,
    SELECTED
}
