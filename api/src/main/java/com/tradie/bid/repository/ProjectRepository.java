package com.tradie.bid.repository;

import com.tradie.bid.modal.BidStatus;
import com.tradie.bid.modal.Project;
import com.tradie.bid.modal.ProjectStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findByLastBidDateLessThanEqual(LocalDate date);

    @Modifying
    @Query("update Project p set p.status = ?1 where p.id = ?2")
    void updateProjectStatus(ProjectStatus status, Long id);
}
