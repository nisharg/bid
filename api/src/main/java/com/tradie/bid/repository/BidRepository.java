package com.tradie.bid.repository;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.modal.BidStatus;
import com.tradie.bid.modal.ProjectStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BidRepository extends JpaRepository<Bid, Long> {
    List<Bid> findByProjectId(Long projectId);

    List<Bid> findByProjectLastBidDateLessThanEqualAndProjectStatusIs(LocalDate date, ProjectStatus status);

    @Modifying
    @Query("update Bid b set b.status = ?1 where b.id = ?2")
    void updateBidStatus(BidStatus status, Long id);
}

