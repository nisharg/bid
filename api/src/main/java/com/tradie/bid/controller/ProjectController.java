package com.tradie.bid.controller;

import com.tradie.bid.modal.Project;
import com.tradie.bid.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@RequiredArgsConstructor
@Validated
public class ProjectController {
    private final ProjectService projectService;

    @GetMapping
    public Page<Project> all(@RequestParam(required = false, defaultValue = "") String status,
                             @RequestParam(required = false, defaultValue = "0") Integer page,
                             @RequestParam(required = false, defaultValue = "50") Integer rowCount) {
        return projectService.find(status, page, rowCount);
    }

    @PostMapping
    public Project createProject(@Valid @RequestBody Project project) {
        return projectService.save(project);
    }

    @PutMapping("/{id}")
    public Project updateProject(@Valid @RequestBody Project project, @NotNull @PathVariable Long id) {
        return projectService.update(id, project);
    }

    @GetMapping("{id}")
    public Project get(@NotNull @PathVariable Long id) {
        return projectService.get(id);
    }
}
