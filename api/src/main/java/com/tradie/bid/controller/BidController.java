package com.tradie.bid.controller;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.service.BidService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Validated
public class BidController {
    private final BidService bidService;

    @GetMapping("/projects/{projectId}/bids")
    public List<Bid> allBids(@NotNull @PathVariable Long projectId) {
        return bidService.all(projectId);
    }

    @PostMapping("/projects/{projectId}/bids")
    public Bid newBid(@Valid @RequestBody Bid bid, @NotNull @PathVariable Long projectId) {
        return bidService.save(projectId, bid);
    }

    @PutMapping("/bids/{id}")
    public Bid updateBid(@Valid @RequestBody Bid bid, @NotNull @PathVariable Long id) {
        return bidService.update(id, bid);
    }
}
