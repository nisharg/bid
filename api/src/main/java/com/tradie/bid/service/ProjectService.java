package com.tradie.bid.service;

import com.tradie.bid.modal.Project;
import com.tradie.bid.modal.ProjectStatus;
import com.tradie.bid.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {
    private final ProjectRepository projectRepository;

    public Project save(Project project) {
        project.setStatus(ProjectStatus.ACTIVE);
        return projectRepository.save(project);
    }

    @Transactional
    public Project update(Long id, Project project) {
        Project currentProject = projectRepository.findById(id).orElseThrow(() -> new RuntimeException("Project not found"));

        if (currentProject.getStatus() != ProjectStatus.ACTIVE) {
            throw new RuntimeException("Project can not be updated");
        }

        currentProject.setTitle(project.getTitle());
        currentProject.setDescription(project.getDescription());
        currentProject.setHours(project.getHours());
        currentProject.setLastBidDate(project.getLastBidDate());

        return projectRepository.save(currentProject);
    }

    public Page<Project> find(String status, Integer page, Integer rowCount) {
        return projectRepository.findAll(PageRequest.of(page, rowCount));
    }

    public Project get(Long id) {
        return projectRepository.findById(id).orElseThrow(() -> new RuntimeException("Project not found"));
    }
}
