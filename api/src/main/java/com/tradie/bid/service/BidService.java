package com.tradie.bid.service;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.modal.BidStatus;
import com.tradie.bid.modal.Project;
import com.tradie.bid.repository.BidRepository;
import com.tradie.bid.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BidService {
    private final BidRepository bidRepository;
    private final ProjectRepository projectRepository;

    public List<Bid> all(Long projectId) {
        return bidRepository.findByProjectId(projectId);
    }

    public Bid save(Long projectId, Bid bid) {
        Project project = projectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("Project not found"));

        bid.setProject(project);
        bid.setStatus(BidStatus.ACTIVE);

        return bidRepository.save(bid);
    }

    @Transactional
    public Bid update(Long id, Bid bid) {
        Bid currentBid = bidRepository.findById(id).orElseThrow(() -> new RuntimeException("Bid not found"));

        currentBid.setFixedPrice(bid.getFixedPrice());
        currentBid.setHourlyPrice(bid.getHourlyPrice());

        return bidRepository.save(currentBid);
    }
}
