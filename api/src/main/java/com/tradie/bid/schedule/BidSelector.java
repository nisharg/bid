package com.tradie.bid.schedule;

import com.tradie.bid.modal.Bid;
import com.tradie.bid.modal.BidStatus;
import com.tradie.bid.modal.Project;
import com.tradie.bid.modal.ProjectStatus;
import com.tradie.bid.repository.BidRepository;
import com.tradie.bid.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class BidSelector {

    private final ProjectRepository projectRepository;
    private final BidRepository bidRepository;

    @Scheduled(fixedRate = 10000)
    @Transactional
    public void schedule() {
        List<Bid> bids = bidRepository.findByProjectLastBidDateLessThanEqualAndProjectStatusIs(LocalDate.now(), ProjectStatus.ACTIVE);

        if (bids.isEmpty()) return;

        HashMap<Project, Bid> projects = new HashMap<>();
        bids.forEach(bid -> {
            Project project = bid.getProject();
            if (!projects.containsKey(project)) {
                projects.put(project, bid);
                return;
            }

            Bid currentBid = projects.get(project);

            if (calculateFinalPrice(project.getHours(), bid) < calculateFinalPrice(project.getHours(), currentBid)) {
                projects.put(project, bid);
            }
        });

        Collection<Bid> selectedBids = projects.values();

        bids.forEach(bid -> {
            if (selectedBids.contains(bid)) {
                bidRepository.updateBidStatus(BidStatus.SELECTED, bid.getId());
            } else {
                bidRepository.updateBidStatus(BidStatus.REJECTED, bid.getId());
            }
        });

        projects.keySet().forEach(p -> projectRepository.updateProjectStatus(ProjectStatus.CLOSED, p.getId()));
    }

    private double calculateFinalPrice(int hours, Bid bid) {
        double fixedPrice = Optional.ofNullable(bid.getFixedPrice()).orElse(0.0);
        double hourlyPrice = Optional.ofNullable(bid.getHourlyPrice()).orElse(0.0);
        return (hourlyPrice * hours) + fixedPrice;
    }
}
