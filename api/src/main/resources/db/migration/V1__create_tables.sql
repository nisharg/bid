CREATE TABLE project (
     id                       bigint          NOT NULL AUTO_INCREMENT,
     title                    varchar(100)    NOT NULL,
     description              varchar(1000)   NOT NULL,
     hours                    int             NOT NULL,
     last_bid_date            datetime        NOT NULL,
     status                   varchar(15)     NOT NULL,
     PRIMARY KEY (id)
);

CREATE TABLE bid (
     id                       bigint          NOT NULL AUTO_INCREMENT,
     project_id               bigint          NOT NULL,
     fixed_price              decimal(15,2)   NULL,
     hourly_price             decimal(15,2)   NULL,
     status                   varchar(15)     NOT NULL,
     PRIMARY KEY (id)
);