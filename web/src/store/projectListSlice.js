import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import ProjectApi from "../utils/api";

export const getProjects = createAsyncThunk(
  'projects',
  async (args, { getState, requestId }) => {
    return await ProjectApi.getProjects(args.page, args.rowCount)
  }
)

const projectListSlice = createSlice({
  name: 'projects',
  initialState: {
    entities: [],
    page: 0,
    rowCount: 20,
    totalElements: 0,
    loading: 'idle'
  },
  reducers: {
    changePage(state, action) {
      state.page = action.payload
    }
  },
  extraReducers: {
    [getProjects.pending]: (state) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.entities = []
      }
    },
    [getProjects.fulfilled]: (state, action) => {
      if (state.loading === 'pending') {
        state.loading = 'idle'
        state.entities = action.payload.content
        state.totalElements = action.payload.totalElements
      }
    },
    [getProjects.rejected]: (state, action) => {
      if (state.loading === 'pending' ) {
        state.loading = 'idle'
        state.error = action.error
      }
    }
  },
})

export const { changePage } = projectListSlice.actions
export default projectListSlice.reducer