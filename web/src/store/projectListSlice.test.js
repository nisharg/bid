import reducer, {getProjects} from './projectListSlice'
import ProjectApi from "../utils/api";
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

jest.mock('../utils/api');

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

describe("projectListSlice", () => {
    test('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(
            {
                entities: [],
                page: 0,
                rowCount: 20,
                totalElements: 0,
                loading: 'idle'
            }
        )
    })

    it('calls the api correctly', async () => {
        const initialState = {}
        const store = mockStore(initialState)
        ProjectApi.getProjects.mockResolvedValue([]);
        await store.dispatch(getProjects(0, 20));
        expect(ProjectApi.getProjects).toHaveBeenCalledTimes(1)
      });
})