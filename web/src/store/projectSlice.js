import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import ProjectApi from "../utils/api";

export const getProject = createAsyncThunk(
  'project',
  async (args, { getState, requestId }) => {
    const project = await ProjectApi.getProject(args.id)
    if (args.withBids) {
        const bids = await ProjectApi.getBids(args.id)
        project.bids = bids
    }
    return project
  }
)

const projectSlice = createSlice({
  name: 'project',
  initialState: {
    entity: null,
    loading: 'idle'
  },
  reducers: {
  },
  extraReducers: {
    [getProject.pending]: (state) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.entity = null
      }
    },
    [getProject.fulfilled]: (state, action) => {
      if (state.loading === 'pending') {
        state.loading = 'idle'
        state.entity = action.payload
      }
    },
    [getProject.rejected]: (state, action) => {
      if (state.loading === 'pending' ) {
        state.loading = 'idle'
        state.error = action.error
      }
    }
  },
})

export default projectSlice.reducer