import * as React from 'react'
import * as yup from "yup";
import {Formik, ErrorMessage} from "formik";
import clsx from "clsx";
import {useState} from "react";
import PageHeader from "../components/PageHeader";
import ProjectApi from '../utils/api';
import Message from '../components/Message';
import DatePicker from '../components/DatePicker';
import {format} from "date-fns";

const validationsForm = {
  title: yup
    .string()
    .required("Title is required"),
  description: yup
    .string()
    .required("Description is required"),
  hours: yup
    .number("Hours must be grather then 0")
    .required("Hours is required")
    .positive()
    .integer(),
  lastBidDate: yup
    .string()
    .required("Last Bid Date is required")
};

const AddProject = (props) => {
  const initialValues = {
    title: '',
    description: '',
    hours: '',
    lastBidDate: ''
  }

  const onSubmit = async (project) => {
    return await ProjectApi.addProject(project)
  }

  return (
    <ProjectForm initialValues={initialValues} onSubmit={onSubmit} op="New"/>
  )
}

const ProjectForm = (props) => {
  const { initialValues, onSubmit, op } = props
  const [message, setMessage] = useState()
  const minDate = new Date();
  minDate.setDate(minDate.getDate() + 1);

  return (
    <>
      <PageHeader>{op} Project</PageHeader>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={yup.object().shape(validationsForm)}
        onSubmit={async (values, actions) => {
          const project = {
            title: values.title,
            description: values.description,
            hours: values.hours,
            lastBidDate: format(values.lastBidDate, "dd/MM/yyyy")
          }
          const response = await onSubmit(project)
          if (response.errors) {
            setMessage({type: 'danger', message: response.errors.join(" ")})
          } else {
            actions.resetForm(initialValues)
            setMessage({type: 'success', message: `Project added.`})
          }
          actions.setSubmitting(false)
        }}
      >
        {props => (
          <form noValidate autoComplete="off" onSubmit={props.handleSubmit}>
            <Message message={message} />

            <div className="mb-3">
              <label htmlFor="name" className="form-label">Title</label>
              <input type="text"
                    className={clsx(["form-control", (props.touched.title && props.errors.title) && "is-invalid"])}
                    id="title"
                    onChange={props.handleChange('title')}
                    value={props.values.title}
                    onBlur={props.handleBlur} />
              <ErrorMessage component="div" name="title" className="invalid-feedback" />
            </div>
            <div className="mb-3">
              <label htmlFor="glCode" className="form-label">Description</label>
              <textarea className={clsx(["form-control", (props.touched.description && props.errors.description) && "is-invalid"])}
                    id="description"
                    onChange={props.handleChange('description')}
                    value={props.values.description}
                    onBlur={props.handleBlur}
                    rows="5" />
              <ErrorMessage component="div" name="description" className="invalid-feedback" />
            </div>
            <div className="mb-3">
              <label htmlFor="glCode" className="form-label">Hours</label>
              <input type="text"
                    className={clsx(["form-control", (props.touched.hours && props.errors.hours) && "is-invalid"])}
                    id="hours"
                    onChange={props.handleChange('hours')}
                    value={props.values.hours}
                    onBlur={props.handleBlur} />
              <ErrorMessage component="div" name="hours" className="invalid-feedback" />
            </div>
            <div className="mb-3">
              <label htmlFor="glCode" className="form-label">Last Bid Date</label>
              <DatePicker id="lastBidDate" className="form-control" name="lastBidDate" minDate={minDate} />
              <ErrorMessage component="div" name="lastBidDate" className="invalid-feedback" />
            </div>
            <button type="submit" className="btn btn-primary" disabled={props.isSubmitting}>
              {props.isSubmitting && (<span className="spinner-border spinner-border-sm me-2" role="status" />)}
              {op} Project
            </button>
          </form>
        )}
      </Formik>
    </>
  );
};

export { AddProject }