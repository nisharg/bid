import * as React from 'react'
import {useHistory, useParams} from "react-router-dom";
import PageHeader from "../components/PageHeader";
import {getProject} from "../store/projectSlice";
import {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Loading from '../components/Loading';

const ProjectViewTradie = () => {
  const { id } = useParams();
  const { entity, loading } = useSelector((state) => state.project)

  const history = useHistory()
  const dispatch = useDispatch()

  useEffect(() => {
    (async () => {
      await dispatch(getProject({id}))
    })()
  }, [id])

  const addBid = () => {
    history.push(`/project/${id}/bid`)
  }

  return (
    <div>
      {loading === 'pending' && (<Loading />)}

      {entity && (
        <div>
            <PageHeader>{entity.title}</PageHeader>
            <div>Description</div>
            <div className="mb-3">{entity.description}</div>
            <div>Hours</div>
            <div className="mb-3">{entity.hours}</div>
            <div>Last Bid Date</div>
            <div className="mb-3">{entity.lastBidDate}</div>
            {entity.status === "ACTIVE" && (<button type="button" className="btn btn-primary" onClick={addBid}>Bid on Project</button>)}
        </div>
      )}
    </div>
  )
}

export default ProjectViewTradie