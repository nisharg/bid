import * as React from 'react'
import {useParams} from "react-router-dom";
import PageHeader from "../components/PageHeader";
import {getProject} from "../store/projectSlice";
import {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Loading from '../components/Loading';

const ProjectViewCustomer = () => {
  const { id } = useParams();
  const { entity, loading } = useSelector((state) => state.project)

  const dispatch = useDispatch()

  useEffect(() => {
    (async () => {
      await dispatch(getProject({id: id, withBids: true}))
    })()
  }, [id])

  return (
    <div>
      {loading === 'pending' && (<Loading />)}

      {entity && (
        <div>
            <PageHeader>{entity.title}</PageHeader>
            <div>Description</div>
            <div className="mb-3">{entity.description}</div>
            <div>Hours</div>
            <div className="mb-3">{entity.hours}</div>
            <div>Last Bid Date</div>
            <div className="mb-3">{entity.lastBidDate}</div>
            <div>Status</div>
            <div className="mb-3">{entity.status}</div>
            {(entity.bids && entity.bids.length > 0) && (
              <table className="table">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Price</td>
                    <td>Status</td>
                  </tr>
                </thead>
                <tbody>
                  {entity.bids.map((bid) => (
                    <tr key={bid.id}>
                      <td>{bid.id}</td>
                      <td><Price fixedPrice={bid.fixedPrice} hourlyPrice={bid.hourlyPrice} /></td>
                      <td>{bid.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            )}
        </div>
      )}
    </div>
  )
}

const Price = (props) => {
  const { hourlyPrice, fixedPrice } = props

  if (hourlyPrice) return (<>${hourlyPrice} per Hour</>)
  if (fixedPrice) return (<>${fixedPrice} Fixed Cost</>)
}

export default ProjectViewCustomer
