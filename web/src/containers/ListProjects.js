import * as React from 'react'
import {useHistory} from "react-router-dom";
import PageHeader from "../components/PageHeader";
import {getProjects} from "../store/projectListSlice";
import {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changePage} from "../store/projectListSlice"; 
import Pagination from '../components/Pagination';
import clsx from "clsx";
import Loading from '../components/Loading';

const ListProjects = () => {
  const { entities, page, rowCount, totalElements, loading } = useSelector((state) => state.projects)

  const history = useHistory()
  const dispatch = useDispatch()

  useEffect(() => {
    (async () => {
      await dispatch(getProjects({page, rowCount}))
    })()
  }, [page, rowCount])

  const handlePageChange = (p) => {
    dispatch(changePage(p))
  };

  return (
    <div>
      <PageHeader>Projects</PageHeader>

      <div className="d-grid gap-2 mb-3">
        <button className="btn btn-primary btn-lg" type="button" onClick={() => history.push('/add')}>New Project</button>
      </div>

      {loading === 'pending' && (<Loading />)}

      {(entities && entities.length) > 0 && (
        <>
          {entities.map((project) => (
            <div className={clsx(["card mb-3", (project.status !== "ACTIVE") && "bg-light"])} key={project.id}>
              <div className="card-body">
                <h4 className="card-title text-truncate">{project.title}</h4>
                <p className="card-text text-truncate">{project.description}</p>
                <a href="#" className="card-link" onClick={() => history.push(`/project/${project.id}/tradie`)}>Tradie View</a>
                <a href="#" className="card-link" onClick={() => history.push(`/project/${project.id}/customer`)}>Customer View</a>
              </div>
            </div>
          ))}

          <Pagination onPageChange={handlePageChange}
                      totalElements={totalElements}
                      pageSize={rowCount}
                      page={page} />
        </>
      )}
    </div>
    
  )
}

export default ListProjects