import React from "react";
import { render, screen, waitFor } from "../test-util";

import {AddBid, BidForm} from "./AddBid";

import userEvent from '@testing-library/user-event'
import ProjectApi from '../utils/api'

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: () => ({
        projectId: 1
    })
}));

jest.mock('../utils/api', () => ({
    addBid: jest.fn()
}))

describe("AddBid", () => {
    test("submit with hourly price AddBid", async () => {
        ProjectApi.addBid.mockResolvedValue({});
        render(<AddBid />)
        userEvent.type(screen.getByLabelText(/hourly price/i), '23')

        userEvent.click(screen.getByText("New"))

        await waitFor(() => {
            expect(ProjectApi.addBid).toHaveBeenCalledWith(1, {
                fixedPrice: '',
                hourlyPrice: '23'
            })
        })
    })

    test("submit with fixed price AddBid", async () => {
        ProjectApi.addBid.mockResolvedValue({});
        render(<AddBid />)
        userEvent.type(screen.getByLabelText(/fixed price/i), '200')

        userEvent.click(screen.getByText("New"))

        await waitFor(() => {
            expect(ProjectApi.addBid).toHaveBeenCalledWith(1, {
                fixedPrice: '200',
                hourlyPrice: ''
            })
        })
    })

    test("submit with no price AddBid", async () => {
        ProjectApi.addBid.mockResolvedValue({});
        render(<AddBid />)
        userEvent.click(screen.getByText("New"))

        await waitFor(() => {
            expect(ProjectApi.addBid).toHaveBeenCalledTimes(0)
        })
    })

    test("error response AddBid", async () => {
        ProjectApi.addBid.mockResolvedValue({errors: [ "test - error" ]});
        render(<AddBid />)
        userEvent.type(screen.getByLabelText(/fixed price/i), '200')

        userEvent.click(screen.getByText("New"))

        await waitFor(() => {
            expect(ProjectApi.addBid).toHaveBeenCalledTimes(1)
            expect(screen.getByText("test - error")).toBeInTheDocument()
        })
    })
})