import * as React from 'react'
import {useParams} from "react-router-dom";
import * as yup from "yup";
import {Formik, ErrorMessage} from "formik";
import clsx from "clsx";
import {useState} from "react";
import PageHeader from "../components/PageHeader";
import ProjectApi from '../utils/api';
import Message from '../components/Message';

const validationsForm = {
  hourlyPrice: yup
  .number()
  .positive(),
  fixedPrice: yup
  .number()
  .positive()
};

const AddBid = () => {
  const { projectId } = useParams();
  const initialValues = {
    hourlyPrice: '',
    fixedPrice: ''
  }

  const onSubmit = async (bid) => {
    return await ProjectApi.addBid(projectId, bid)
  }

  return (
    <BidForm initialValues={initialValues} onSubmit={onSubmit} op="New" />
  )
}

const BidForm = (props) => {
  const { initialValues, onSubmit, op } = props
  const [message, setMessage] = useState()

  return (
    <>
      <PageHeader>{op} Bid</PageHeader>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={yup.object().shape(validationsForm)}
        onSubmit={async (values, actions) => {
          const bid = {
            hourlyPrice: values.hourlyPrice,
            fixedPrice: values.fixedPrice
          }
          const response = await onSubmit(bid)
          if (response.errors) {
            setMessage({type: 'danger', message: response.errors.join(" ")})
          } else {
            actions.resetForm(initialValues)
            setMessage({type: 'success', message: `Bid added`})
          }
          actions.setSubmitting(false)
        }}
      >
        {props => (
          <form noValidate autoComplete="off" onSubmit={props.handleSubmit}>
            <Message message={message} />

            <div className="mb-3">
              <label htmlFor="hourlyPrice" className="form-label">Hourly Price</label>
              <div className="input-group mb-3">
                <span className="input-group-text">$</span>
                <input type="text"
                      className={clsx(["form-control", (props.touched.hourlyPrice && props.errors.hourlyPrice) && "is-invalid"])}
                      id="hourlyPrice"
                      onChange={props.handleChange('hourlyPrice')}
                      value={props.values.hourlyPrice}
                      onBlur={props.handleBlur} />
              </div>
              <ErrorMessage component="div" name="hourlyPrice" className="invalid-feedback" />
            </div>
            <div className="mb-3">
              <label htmlFor="fixedPrice" className="form-label">Fixed Price</label>
              <div className="input-group mb-3">
                <span className="input-group-text">$</span>
                <input type="text"
                      className={clsx(["form-control", (props.touched.fixedPrice && props.errors.fixedPrice) && "is-invalid"])}
                      id="fixedPrice"
                      onChange={props.handleChange('fixedPrice')}
                      value={props.values.fixedPrice}
                      onBlur={props.handleBlur} />
              </div>
              <ErrorMessage component="div" name="fixedPrice" className="invalid-feedback" />
            </div>
            <button type="submit" className="btn btn-primary" disabled={props.isSubmitting}>
              {props.isSubmitting && (<span className="spinner-border spinner-border-sm me-2" role="status" />)}
              {op}
            </button>
          </form>
        )}
      </Formik>
    </>
  );
};

export { AddBid }
