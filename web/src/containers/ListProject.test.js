import React from "react";
import { render, screen, waitFor } from "../test-util";
import ListProjects from "./ListProjects";
import userEvent from '@testing-library/user-event'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import {Router} from 'react-router-dom'
import {createMemoryHistory} from 'history'

export const handlers = [
    rest.get('http://localhost/api/projects', (req, res, ctx) => {
      return res(ctx.json({
        "content": [
            {
                "id": 3,
                "title": "title-header",
                "description": "description-short",
                "hours": 10,
                "lastBidDate": "2022-02-10",
                "status": "ACTIVE"
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "empty": true,
                "unsorted": true
            },
            "pageNumber": 0,
            "pageSize": 50,
            "offset": 0,
            "unpaged": false,
            "paged": true
        },
        "last": true,
        "totalElements": 1,
        "totalPages": 1,
        "numberOfElements": 1,
        "size": 20,
        "number": 0,
        "first": true,
        "sort": {
            "sorted": false,
            "empty": true,
            "unsorted": true
        },
        "empty": false
    }), ctx.delay(150))
    })
]
const server = setupServer(...handlers)
beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe("ListProjects", () => {
    test("show list", async () => {
        const history = createMemoryHistory()
        const pushSpy = jest.spyOn(history, 'push')


        render(<Router history={history}><ListProjects /></Router>)
        expect(await screen.findByText(/title-header/i)).toBeInTheDocument()
        expect(await screen.findByText(/description-short/i)).toBeInTheDocument()
        
        userEvent.click(screen.getByText("Tradie View"))
        await waitFor(() =>
            expect(pushSpy).toHaveBeenCalledWith("/project/3/tradie")
        )

        userEvent.click(screen.getByText("Customer View"))
        await waitFor(() =>
            expect(pushSpy).toHaveBeenCalledWith("/project/3/customer")
        )
    })
})