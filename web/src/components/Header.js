import * as React from 'react'
import {useHistory} from "react-router-dom";

const Header = () => {
  const history = useHistory();

  return (
    <nav className="navbar navbar-expand navbar-dark bg-primary mb-3">
      <div className="container-sm">
        <button type="button"
                className="btn btn-link navbar-brand"
                onClick={(event) => history.push('/')}>Project Bid App</button>
      </div>
    </nav>
  )
}

export default Header