import * as React from 'react'
import clsx from 'clsx';

const Pagination = (props) => {
  const {page, onPageChange, totalElements, pageSize } = props

  const isLastPage = () => ((Math.ceil(totalElements/pageSize) - 1)  === page)
  const pageStartElement = () => (page * pageSize + 1)
  const pageEndElement = () => (isLastPage ? totalElements : (page * pageSize + pageSize))

  return (
    <div className="d-flex flex-row-reverse">
      <ul className="pagination">
        <li className={clsx("page-item", page === 0 && "disabled")} data-testid="project-previous">
          <button className="page-link" data-testid="previous-button" onClick={() => onPageChange(page-1)}>
            <span>&laquo;</span>
          </button>
        </li>
        <li className={clsx("page-item", isLastPage() && "disabled")} data-testid="project-next">
          <button className="page-link" data-testid="next-button" onClick={() => onPageChange(page+1)}>
            <span>&raquo;</span>
          </button>
        </li>
      </ul>
      <span className="me-4 pt-2">{pageStartElement()}-{pageEndElement()} of {totalElements}</span>
    </div>
  )
}

export default Pagination;