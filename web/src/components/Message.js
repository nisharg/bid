import * as React from 'react'
import clsx from 'clsx';

const Message = (props) => {
  const { message } = props

  if (!message) return (<></>)

  return (
      <div className={clsx(["alert", `alert-${message.type}`])} role="alert">
        {message.message}
      </div>
  )
}

export default Message;