import React from "react";
import { render, screen } from "../test-util";

import Header from "./Header";

describe("Header", () => {
    test("render Header", () => {
        render(<Header />);
        expect(screen.queryByText(/Project Bid App/)).toBeInTheDocument()
    })
})