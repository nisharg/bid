import React from "react";
import { useField, useFormikContext } from "formik";
import WDatePicker from 'react-date-picker';

export const DatePickerField = ({ ...props }) => {
  const { setFieldValue } = useFormikContext();
  const [field] = useField(props);
  return (
    <WDatePicker
      {...field}
      {...props}
      clearIcon=""
      calendarIcon={<i className="bi bi-calendar" />}
      format="dd/MM/yyyy"
      selected={(field.value && new Date(field.value)) || null}
      disabled={props.disabled}
      onChange={val => {
        setFieldValue(field.name, val);
      }}
    />
  );
};

export default DatePickerField;
