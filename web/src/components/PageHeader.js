import * as React from 'react'

const PageHeader = (props) => {
  return (<h1 className="mb-3 mt-4">{props.children}</h1>)
}

export default PageHeader;