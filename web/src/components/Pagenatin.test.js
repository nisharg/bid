import React from "react";
import { render, screen, waitFor } from "../test-util";
import userEvent from '@testing-library/user-event'

import Pagination from "./Pagination";

describe("Pagination", () => {
    const onPageChange = jest.fn()
    beforeEach(() => {
        onPageChange.mockReset()
    })

    test("pagination first page disabled", async () => {
        const props = {
            page: 0,
            onPageChange: onPageChange,
            totalElements: 20,
            pageSize: 10
        }
        render(<Pagination {...props} />);
        expect(screen.getByTestId('project-previous')).toHaveClass('disabled')
        expect(screen.getByTestId('project-next')).toHaveClass('page-item', { exact: true})

        userEvent.click(screen.getByTestId("next-button"))
        expect(onPageChange).toHaveBeenCalledWith(1)
    })

    test("pagination last page disabled", async () => {
        const props = {
            page: 1,
            onPageChange: onPageChange,
            totalElements: 20,
            pageSize: 10
        }
        render(<Pagination {...props} />);
        expect(screen.getByTestId('project-previous')).toHaveClass('page-item', { exact: true})
        expect(screen.getByTestId('project-next')).toHaveClass('disabled')

        userEvent.click(screen.getByTestId("previous-button"))
        expect(onPageChange).toHaveBeenCalledWith(0)
    })
})