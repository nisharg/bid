import React from "react";
import { render, screen } from "../test-util";

import PageHeader from "./PageHeader";

describe("PageHeader", () => {
    test("render PageHeader", () => {
        render(<PageHeader>Test</PageHeader>);
        expect(screen.getByText(/Test/i)).toBeInTheDocument();

        render(<PageHeader>Page Header</PageHeader>);
        expect(screen.getByText(/Page Header/)).toBeInTheDocument();
    })
})