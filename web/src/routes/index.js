import * as React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Header from '../components/Header'
import {AddProject} from '../containers/AddProject'
import {AddBid} from '../containers/AddBid'
import ListProjects from '../containers/ListProjects'
import ProjectViewTradie from '../containers/ProjectViewTradie'
import ProjectViewCustomer from '../containers/ProjectViewCustomer'

export const AppRouter = () => {
  return (
    <Router>
      <Header />
      <div className='container-sm'>
      <Switch>
          <Route path="/add" component={AddProject} />
          <Route path="/project/:projectId/bid" component={AddBid} />
          <Route path="/project/:id/tradie" component={ProjectViewTradie} />
          <Route path="/project/:id/customer" component={ProjectViewCustomer} />
          <Route path="/" component={ListProjects} />
      </Switch>
      </div>
    </Router>
  )
}
