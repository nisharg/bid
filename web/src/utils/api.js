import axios from "axios"
import _ from 'lodash'

const API_BASE_URL = '/api/';
const REQUEST_TIMEOUT = 25000;

const axiosDefaultConfig = {
  baseURL: API_BASE_URL,
  withCredentials: true,
  timeout: REQUEST_TIMEOUT,
};

export const mapResponseToData = (response) => {
  const { data } = response;
  return data || response;
};

const callApi = async (options = {}, requestParams) => {
  try {
    const response = await axios({
      ...axiosDefaultConfig,
      headers: {
        'Content-Type': 'application/json'
      },
      ...options,
      ...requestParams
    })

    return mapResponseToData(response)
  } catch (error) {
    const backendErrors = await getErrorResponse(error);
    console.error("Error caling api", backendErrors)
    return { errors: backendErrors || [ "Something went wrong. Please try again after sometime."] }
  }
}

const getErrorResponse = async (error) => {
  if (!error.response) {
    return [ "Something went wrong. Please try again after sometime."]
  }
  const responseErrors = _.property(['response', 'data', 'errors'])
  const content = responseErrors(error)
  
  return content;
}

const getProjects = (page, rowCount) => {
  return callApi({url: 'projects'}, {params: {page, rowCount}})
}

const getProject = (id) => {
  return callApi({url: `projects/${id}`})
}

const addProject = (data) => {
  return callApi({url:'projects', method: 'post'}, {data: data})
}

const addBid = (projectId, data) => {
  return callApi({url:`projects/${projectId}/bids`, method: 'post'}, {data: data})
}

const getBids = (id) => {
  return callApi({url: `projects/${id}/bids`})
}

const ProjectApi = {
  getProjects,
  addProject,
  getProject,
  addBid,
  getBids
}
  
export default ProjectApi