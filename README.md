# Tradie Project Bid 

## How to run

### web
```
cd web
npm start
```

### api
```
cd api
./gradlew bootRun
```

Navigate to http://localhost:3000